﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using D12;

namespace SnowCraft.Controllers
{
    [ApiController]
    [Route("/")]
    public class ShootingController : ControllerBase
    {
        private readonly ILogger<ShootingController> _logger;

        public ShootingController(ILogger<ShootingController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            var rng = new Random();
            var d12Core = new d12_core();
            return d12Core.GetResult(0,0);
        }


    }
}
