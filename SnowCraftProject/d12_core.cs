﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D12
{
	#region Перечисления и прочие типы
	/// <summary>
	/// Тип предмета: пусто, броня, оружие, модуль
	/// </summary>
	public enum Item_type { empty = 0, armour = 1, weapon = 2, module = 3 };

	/// <summary>
	/// Класс брони: отсутствует, легая, средняя, тяжелая, непробиваемая (энергетическая)
	/// </summary>
	public enum Armour_class { non = 0, light = 1, medium = 2, heavy = 3, invul = 4 };
	/// <summary>
	/// Класс тяжести оружия: без класс (не оружие), легкое, крупнокалиберное, тяжелое/взрвное, энергетическое
	/// </summary>
	public enum Weapon_class { non = 0, light = 1, medium = 2, heavy = 3, invul = 4 };

	/// <summary>
	/// Принцип действия: не оружие, нож, пистолет, карабин, автоматическое, пушка, пусковая установка, устройство (байда)
	/// </summary>
	public enum Weapon_type { non, knife, pistol, carabine, auto, cannon, launcher, device };
	/// <summary>
	/// Тип бонуса: пусто, ХП, точность (+Д12), Прицеливание, класс брони, класс оружия, дамаг, дальность, скорость, реакция, обзор, мораль, спец.правила
	/// </summary>
	public enum Bonus_type { non, hp, accuraty, aim, armr_cls, weap_class, damage, range, skill, will, speed, reaction, sight, qactivitems, moral, special };
	/// <summary>
	/// Тип боеприпаса (чем сносит хиты): ничем, нож/лезвие, пуля, шрапнель, лазер, плазма, граната, рокета, взрывная волна/волна
	/// </summary>
	public enum Round_type { non, blade, bullet, grip, laser, plasma, granade, rocket, shockwave };

	/// <summary>
	///Рукость: никакая, одноручная, двуручная
	/// </summary>
	public enum Hands { non, onehanded, twohended };
	/// <summary>
	///Специальные правила, которые предоставляет предмет
	/// (Обработка самого названия в методе обращения к юниту):
	/// пусто, невидимость, ночное зрение, доп.слот действия, ...
	/// </summary>
	public enum Special_rls { non, invis, nightsight, actionslot, dodge };
	/// <summary>
	/// Доступные действия юнита: ничего, ходить, бежать, использовать, ударить, стрелять
	/// </summary>
	public enum Actions { non, move, run, use, blow, shoot };
	#endregion Перечисления и прочие типы

	/// <summary>
	/// Класс описывающий еправила системы D12
	/// </summary>
	public class d12_core
	{
		/*
          П - пусто
          Г - голова
          Р - рука
          К - корпус
          Н - нога

          На кубике - Варианты
          1 - П-П
          2 - Г-П
          3 - Г-Г
          4 - Г-К
          5 - Р-Р
          6 - Р-П
          7 - К-Р
          8 - К-К
          9 - К-П
          10 - К-Н
          11 - Н-Н
          12 - Н-П

          При определении результата стрельбы по бронетехнике:
          М - Мимо
          С - Скользящее
          О - Обычное
          К - Критическое      

          На кубике - Варианты
          1 - М-М
          2 - С-М
          3 - О-М
          4 - О-С
          5 - К-С
          6 - К-О
          7 - К-К
          8 - К-М
          9 - О-С
          10 - О-М
          11 - С-М
          12 - М-М

          Кол-во бросаемых кубов определяется типом стрельбы
          Выбираемый вариант определяется типом оружия и наличием перемещений в этот ход.

          Например, снайперская винтовка имеет возможность делать точный выстрел. При точном выстреле бросается 4 куба. Но! Класс оружия - тяжелое. Следовательно, если перемещался в этот ход, то нужно выбирать из худших вариантов. 

          Зависимость точности от движения:

          С - Стреляющий,
          О - Обстрелянный

                         О стоял     О ходил
          С стоял   Любой       Из плохих
          С ходил  Из плохих  Самый плохой

          Тип выстрела:

          Взрыв/шрапнель:  1 куб
          Навскидку:       2 куба
          Обычный:         3 куба
          Прицельный:      4 куба

          Классы оружия по тяжести:
          1 - Легкое, персональное
          2 - Тяжелое, например, СВ, пулеметы, крупно-калиберные пушки
          3 - Артиллерия, например, 150мм, гранатометы, тяжелая плазма.

          По принципу работы:
          Пистолет
          Винтовка
           - СВ
           - Автомат
          Ружье
           - Карабин
           - Дробовик
          Установка
        */

		#region Поля
		// 0 - Г
		// 1 - К
		// 2 - Н
		// 3 - Р
		// 4 - П
		/// <summary>
		/// Базовые результаты:
		///1 - П-П
		///2 - Г-П
		///3 - Г-Г
		///4 - Г-К
		///5 - Р-Р
		///6 - Р-П
		///7 - К-Р
		///8 - К-К
		///9 - К-П
		///10 - К-Н
		///11 - Н-Н
		///12 - Н-П
		/// </summary>
		private List<D12Result> BaseResults = new List<D12Result>()
		{
			new D12Result("Пусто", 4, "Пусто", 4),
			new D12Result("Голова", 0, "Пусто", 4),
			new D12Result("Голова", 0, "Голова", 0),
			new D12Result("Голова", 0, "Корпус", 1),
			new D12Result("Рука", 3, "Рука", 3),
			new D12Result("Рука", 3, "Пусто", 4),
			new D12Result("Корпус", 1, "Рука", 3),
			new D12Result("Корпус", 1, "Корпус", 1),
			new D12Result("Корпус", 1, "Пусто", 4),
			new D12Result("Корпус", 1, "Нога", 2),
			new D12Result("Нога", 2, "Нога", 2),
			new D12Result("Нога", 2, "Пусто", 4)
		};

		/// <summary>
		/// Результаты попаданий по бронетехнике:
		/// 1 - М-М
		/// 2 - С-М
		/// 3 - О-М
		/// 4 - О-С
		/// 5 - К-С
		/// 6 - К-О
		/// 7 - К-К
		/// 8 - К-М
		/// 9 - О-С
		/// 10 - О-М
		/// 11 - С-М
		/// 12 - М-М
		/// Значения:
		///3 - М - Мимо
		///2 - С - Скользящее
		///1 - О - Обычное
		///0 - К - Критическое
		/// </summary>
		private List<D12Result> VehicleResults = new List<D12Result>()
		{
			new D12Result("Мимо", 3, "Мимо", 3),
			new D12Result("Скользящее", 2, "Мимо", 3),
			new D12Result("Обычное", 1, "Мимо", 3),
			new D12Result("Обычное", 1, "Скользящее", 2),
			new D12Result("Критическое", 0, "Скользящее", 2),
			new D12Result("Критическое", 0, "Обычное", 1),
			new D12Result("Критическое", 0, "Критическое", 0),
			new D12Result("Критическое", 0, "Мимо", 3),
			new D12Result("Обычное", 1, "Скользящее", 2),
			new D12Result("Обычное", 1, "Мимо", 3),
			new D12Result("Скользящее", 2, "Мимо", 3),
			new D12Result("Мимо", 3, "Мимо", 3)
		};

		/// <summary>
		/// Результаты пси-каста:
		/// 1 - С*-С*
		/// 2 - С-С*
		/// 3 - М-С
		/// 4 - М-М
		/// 5 - П-М
		/// 6 - П-П
		/// 7 - П*-*
		/// 8 - П-М
		/// 9 - М-М
		/// 10 - М-С
		/// 11 - С-С*
		/// 12 - С*-С*
		/// Значения:
		///4 С* - Себе Крит
		///3 С - Себе
		///2 М - Мимо, кст не состоялся или носит несущественно слабый характер
		///1 П - "Получилось!" (Противнику)
		///0 П* - "Получилось!" Крит
		/// </summary>
		private List<D12Result> PsyResults = new List<D12Result>()
		{
			new D12Result("Себе*", 4, "Себе*", 4),
			new D12Result("Себе", 3, "Себе*", 4),
			new D12Result("Промах", 2, "Себе", 3),
			new D12Result("Промах", 2, "Промах", 2),
			new D12Result("Получилось", 1, "Промах", 2),
			new D12Result("Получилось", 1, "Получилось", 1),
			new D12Result("Получилось*", 0, "Получилось*", 0),
			new D12Result("Получилось", 1, "Промах", 2),
			new D12Result("Промах", 2, "Промах", 2),
			new D12Result("Промах", 2, "Себе", 3),
			new D12Result("Себе", 3, "Себе*", 4),
			new D12Result("Себе*", 4, "Себе*", 4)
		};

		/// <summary>
		/// Отношение к перемещению юнитов
		/// 0 - обы стоят
		/// 1 - стреляющий стоит, обстреляный ходит
		/// 2 - стреляющий ходит, остреляный стоит
		/// 3 - оба ходят
		/// </summary>
		public List<string> Mv_Type { get; } = new List<string>() { "Оба стоят", "Стрелок стоит, обстреляный ходит", "Стрелок ходит, обстреляный стоит", "Оба ходят" };

		/// <summary>
		/// Типа приценивания (определяет кол-во кидаемых кубиков, selecteedindex +1)
		/// 0 - Взрыв/шрапнель
		/// 1 - Навскидку
		/// 2 - Обычное
		/// 3 - Прицельное
		/// </summary>
		public List<string> Aim_Type { get; } = new List<String>() { "Взрыв/шрапнель(1 x D12)", "Навскидку     (2 x D12)", "Обычное       (3 x D12)", "Прицельное    (4 x D12)" };

		/// <summary>
		/// Положение моралей:
		/// 0 - Мораль кастующего ниже
		/// 1 - Морали равны, кастующий - не мастер
		/// 2 - Морали равны, кастующий - мастер
		/// 3 - Мораль кастующего больше
		/// </summary>
		public List<string> Dm_Type { get; } = new List<String>()
		{
			 "Мораль кастующего ниже",
			 "Морали равны, кастующий - не мастер",
			 "Морали равны, кастующий - мастер",
			 "Мораль кастующего больше"
		};

		/// <summary>
		/// ПСИ-навык (уровень)
		/// 0 - Неинициирован
		/// 1 - Посвященный
		/// 2 - Боец
		/// 3 - Магус
		/// </summary>
		public List<string> PSI_skill { get; } = new List<String>()
		{
			"Неинициированный(1 x D12)",
			"Посвященный     (2 x D12)",
			"Боец            (3 x D12)",
			"Магус           (4 x D12)"
		};

		/// <summary>
		/// Тип юнита цели
		/// 0 - пехота
		/// 1 - бронетехника
		/// 2 - пси-сущность
		/// </summary>
		public int TargetType { get; set; }

		/// <summary>
		/// Результаты ПСИ-воздействия:
		///1 - T - Ускорить: юнит получает еще один ход
		///2 - Ps- ПСИ-шит: на юнит кладется отметка щита до конца следующего хода. Для обстрела юнит считается двигавшимтся, юнит не может получать критические паподания.
		///3 - H - Вылечить: восстановить все ОЗ юнита
		///4 - C - Очистить: Снять с юнита одну/все* метку (паника, подавление, отключкаб пси-щит)
		///5 - Sh- Шокировать: следующие 2 хода у юнита доступен только один слот действий (метка подавления)
		///6 - B - Неистовство: юнит тратит все слоты действия на стрельбу по ближайшему юниту (метка паники)
		///7 - R - Бегство: юнит тратитвсе слоты действия на перемещене от противника (метка паники)
		///8 - Rd- Бегство с выбросом оружия (метка паники)
		///9 - S - Ступор: обнулить доступные слоты действия (метка отключки)
		///10- Sd- Ступор с выбросом оружия (метка отключки)
		///11- D - Психосоматический ущерб: нанести противнику 1 дамаг/смертельный дамаг*
		///12- Uc- Контроль разума: следующий ход юнит будет находиться под контролем.
		///Чего?
		///1 - T - Ускорить: юнит получает еще один ход
		///2 - Ps- ПСИ-шит: на юнит кладется отметка щита до конца следующего хода. Для обстрела юнит считается двигавшимтся, юнит не может получать критические паподания.
		///3 - H - Вылечить: восстановить все ОЗ юнита
		///4 - C - Очистить: Снять с юнита одну/все* метку (паника, подавление, отключкаб пси-щит)
		///5 - Sh- Шокировать: следующие 2 хода у юнита доступен только один слот действий (метка подавления)
		///6 - B - Неистовство: юнит тратит все слоты действия на стрельбу по ближайшему юниту (метка паники)
		///7 - R - Бегство: юнит тратитвсе слоты действия на перемещене от противника (метка паники)
		///8 - Rd- Бегство с выбросом оружия (метка паники)
		///9 - S - Ступор: обнулить доступные слоты действия (метка отключки)
		///10- Sd- Ступор с выбросом оружия (метка отключки)
		///11- D - Психосоматический ущерб: нанести противнику 1 дамаг/смертельный дамаг*
		///12- Uc- Контроль разума: следующий ход юнит будет находиться под контролем.
		/// </summary>
		internal List<D12PsiEffect> Psi_effects { get; } = new List<D12PsiEffect>()
		{
			new D12PsiEffect("Ускорить", "T", "Юнит получает еще один слот действия", "Юнит получает еще один ход (Два слота)", 6),
			new D12PsiEffect("ПСИ-шит", "Ps", "Юнит получает отметку щита до конца следующего хода. Для обстрела юнит считается двигавшимтся, юнит не может получать критические паподания",
											  "Юнит получает отметку щита до конца сражения", 5),
			new D12PsiEffect("Вылечить", "H", "Восстановить все ОЗ юнита", "Восстановить все ОЗ юнита и добавить еще столько же ЗО", 4),
			new D12PsiEffect("Очистить", "C", "Снять с юнита одну метку (паника, подавление, отключкаб пси-щит)", "Снять с юнита все метки (паника, подавление, отключкаб пси-щит)", 3),
			new D12PsiEffect("Шокировать", "Sh", "Следующие 2 хода у юнита доступен только один слот действий (метка подавления)",
												 "До конца сражения у юнита доступен только один слот действий (метка подавления)", 2),
			new D12PsiEffect("Неистовство", "B", "Юнит тратит все слоты действия на стрельбу по ближайшему вражескому юниту (метка паники)",
												 "Юнит тратит все слоты действия на стрельбу по любому ближайшему юниту (метка паники)", 1),
			new D12PsiEffect("Бегство", "R", "Юнит получает метку паники и тратит все слоты действия на перемещение от противника (Метка паники)",
											 "Юнит получает метку паники, бросает оружие, и тратит все слоты действия на перемещение от противника (Метка паники)", -1),
			new D12PsiEffect("Бегство с выбросом оружия", "Rd", "Юнит получает метку паники, бросает оружие, и тратит все слоты действия на перемещение от противника (Метка паники)",
																"Юнит получае метку паники, его слоты действия обнуляются до докнца следующего хода (метка паники)", -2),
			new D12PsiEffect("Ступор", "S", "Юнит получает метку папники, его слоты действия обнуляются до докнца следующего хода (метка паники)",
											"Юнит получает метку отключки, роняет оружие, теряет сознание, его слоты действия обнуляются до докнца следующего хода (метка отключки)", -3),
			new D12PsiEffect("Ступор с выбросом оружия", "Sd", "Юнит получает метку отключки, роняет оружие, теряет сознание, его слоты действия обнуляются до докнца следующего хода (метка отключки)",
															   "Юнит теряет 1 хит и сознание (метка отключки)", -4),
			new D12PsiEffect("Психосоматический ущерб", "D", "Юнит теряет 1 хит", "Юнит теряет все хиты", -5),
			new D12PsiEffect("Контроль разума", "Uc", "Юнит получает метку подконтрольного и следующий ход юнит будет находиться под контролем противника",
													  "Юнит получает метку подконтрольного и доконца сражения будет находиться под контролем противника", -6)
		};


		#endregion Поля

		#region Методы
		public d12_core()
		{
			TargetType = 0;
		}

		/// <summary>
		/// Получение строчки с результатом
		/// </summary>
		/// <param name="mv">Отношение к движению (Положение моралей)</param>
		/// <param name="aim">Тип прицеливания (ПСИ-скилл)</param>
		/// <returns>Что вышло в текстовом варианте</returns>
		public string GetResult(int mv, int aim)
		{
			if (aim < 0) return "Выберите тип прицеливания (Aim).";
			if (mv < 0) return "Выберите движение юнитов (Move).";

			StringBuilder sb_res = new StringBuilder();

			//Получаем значения
			List<D12Simpl> bams = GetRmdShooting(mv, aim);
			foreach (D12Simpl D12_bam in bams)
			{
				sb_res.AppendLine(D12_bam.ResType + GetPsiEffect(D12_bam.ResType));
			}

			return sb_res.ToString();
		}

		/// <summary>
		/// Получить результат для рукопашной атаки
		/// </summary>
		/// <param name="a_">Поведение юнитак: 1 - Атакует, 0 - все остальные случае (блок и контр-атака)</param>
		/// <param name="r1_">Показатель реакции активного юнита 1 - определяет порядок выполнения действий</param>
		/// <param name="r2_">Показатель реакции второго юнита 2 </param>
		/// <param name="s_">Навык (Skill - определяет кол-во Д12)</param>
		/// <returns>Список результатов броска</returns>
		public string GetResult(bool a_, int r1_, int r2_, int s_)
		{
			if (r1_ < 0) return "Реакиця неуказана (Reaction 1).";
			if (r2_ < 0) return "Реакиця неуказана (Reaction 2).";
			if (s_ < 0) return "Навык не указан (Skill).";

			StringBuilder sb_res = new StringBuilder();

			//Получаем значения
			List<D12Simpl> bams = GetRmdHTH(a_, r1_, r2_, s_);

			foreach (D12Simpl D12_bam in bams)
			{
				sb_res.AppendFormat("{0:s}, ", D12_bam.ResType);
			}

			return sb_res.ToString();
		}

		/// <summary>
		/// Бросок Д12 (shooting)
		/// </summary>
		/// <param name="mv">отношение к движению (положение морали)</param>
		/// <param name="aim">тип прицеливания (пси-скилл)</param>
		/// <returns>массив результатов броска</returns>
		private List<D12Simpl> GetRmdShooting(int mv, int aim)
		{
			List<D12Simpl> resD12 = new List<D12Simpl>();
			List<int> ress = new List<int>();
			Random rnd12 = new Random();

			if (mv < 0 || aim < 0) return resD12;

			#region Заполняем пространство для Д12
			int d12_count = aim + 1;

			for (int i = 0; i < d12_count; i++)
				ress.Add(rnd12.Next(0, 12));
			#endregion Заполняем пространство для Д12

			#region Выбираем результаты
			switch (TargetType)
			{
				case 2:
					#region пси-сущности
					//Отношение к морали:
					//0 - Мораль катсующего меньше
					//1 - Морали равны, кастующий не мастер
					//2 - Морали равны, кастующий мастер
					//3 - Мораль кастующего больше

					switch (mv)
					{
						case 0://мораль кастующего меньше
							foreach (int i_ in ress)
							{
								resD12.Add(PsyResults[i_].Bad);
							}
							//выбираем один наихудший
							D12Simpl TheWorth = resD12[0];
							foreach (var item in resD12)
							{
								if (item.ResWorth > TheWorth.ResWorth)
								{
									TheWorth = item;
								}
							}
							resD12.Clear();
							resD12.Add(TheWorth);
							break;

						case 1://морали одинаковые - кастующий не мастер
							foreach (int i_ in ress)
							{
								resD12.Add(PsyResults[i_].Bad);
							}
							break;

						case 2://морали одинаковые - кастующий мастер
							foreach (int i_ in ress)
							{
								resD12.Add(PsyResults[i_].Good);
							}
							break;

						case 3://мораль кастующего больше
							foreach (int i_ in ress)
							{
								resD12.Add(PsyResults[i_].Good);
							}
							//выбираем один наилучший
							D12Simpl TheBest = resD12[0];
							foreach (var item in resD12)
							{
								if (item.ResWorth > TheBest.ResWorth)
								{
									TheBest = item;
								}
							}
							resD12.Clear();
							resD12.Add(TheBest);
							break;

						default:
							break;
							#endregion пси-сущности
					}
					break;
				case 1:
					{
						#region бронетехника
						// Отношение к перемещению юнитов
						// 0 - обы стоят
						// 1 - стреляющий стоит, обстреляный ходит
						// 2 - стреляющий ходит, остреляный стоит
						// 3 - оба ходят
						switch (mv)
						{
							case 0:
								foreach (int i_ in ress)
								{
									resD12.Add(VehicleResults[i_].Good);
									resD12.Add(VehicleResults[i_].Bad);
								}
								break;

							case 1:
							case 2:
								foreach (int i_ in ress)
								{
									resD12.Add(VehicleResults[i_].Bad);
								}
								break;

							case 3:
							default:
								foreach (int i_ in ress)
								{
									resD12.Add(VehicleResults[i_].Bad);
								}

								D12Simpl d12TMP = resD12[0];

								for (int i = 1; i < resD12.Count; i++)
								{
									if (resD12[i].ResWorth > d12TMP.ResWorth)
									{
										d12TMP = resD12[i];
									}
								}

								resD12.Clear();
								resD12.Add(d12TMP);
								break;
						}
						#endregion бронетехника
						break;
					}
				case 0:
				default:
					{
						#region пехота
						// Отношение к перемещению юнитов
						// 0 - обы стоят
						// 1 - стреляющий стоит, обстреляный ходит
						// 2 - стреляющий ходит, остреляный стоит
						// 3 - оба ходят
						switch (mv)
						{
							case 0:
								foreach (int i_ in ress)
								{
									resD12.Add(BaseResults[i_].Good);
									resD12.Add(BaseResults[i_].Bad);
								}
								break;

							case 1:
							case 2:
								foreach (int i_ in ress)
								{
									resD12.Add(BaseResults[i_].Bad);
								}
								break;

							case 3:
							default:
								foreach (int i_ in ress)
								{
									resD12.Add(BaseResults[i_].Bad);
								}

								D12Simpl d12TMP = resD12[0];

								for (int i = 1; i < resD12.Count; i++)
								{
									if (resD12[i].ResWorth > d12TMP.ResWorth)
									{
										d12TMP = resD12[i];
									}
								}

								resD12.Clear();
								resD12.Add(d12TMP);
								break;
						}
						#endregion пехота
						break;
					}
			}
			#endregion Выбираем результаты

			return ReduceDoubls(resD12);
		}

		/// <summary>
		/// Бросок Д12 для рукопашной атаки (hth)
		/// </summary>
		/// <param name="a_">1 - атакует, 0 - все остальные случае</param>
		/// <param name="r1_">Реакция первого</param>
		/// <param name="r2_">Реакция второго</param>
		/// <param name="s_">Навык</param>
		/// <returns>Список результатов</returns>
		private List<D12Simpl> GetRmdHTH(bool a_, int r1_, int r2_, int s_)
		{
			List<D12Simpl> resD12 = new List<D12Simpl>();
			List<int> ress = new List<int>();
			Random rnd12 = new Random();

			#region Заполняем пространство для Д12
			int d12_count = s_;

			for (int i = 0; i < d12_count; i++)
				ress.Add(rnd12.Next(0, 12));
			#endregion Заполняем пространство для Д12

			#region Выбираем результаты
			//Нападающий?
			if (a_ || (r1_ > r2_))
			{
				foreach (int i_ in ress)
				{
					resD12.Add(BaseResults[i_].Good);
					resD12.Add(BaseResults[i_].Bad);
				}
			}
			else
			{
				foreach (int i_ in ress)
				{
					resD12.Add(BaseResults[i_].Bad);
				}
			}
			#endregion Выбираем результаты

			return ReduceDoubls(resD12);
		}

		/// <summary>
		/// Убрать повторяющиеся элементы в списке результатов
		/// </summary>
		/// <param name="_res">Проверяемый список</param>
		/// <returns>Готовый список</returns>
		private List<D12Simpl> ReduceDoubls(List<D12Simpl> _res)
		{
			for (int i = 0; i < _res.Count; i++)
			{
				for (int j = i + 1; j < _res.Count; j++)
				{
					if (_res[i].ResWorth == _res[j].ResWorth)
					{
						_res.RemoveAt(j);
						j--;
					}
				}
			}

			return _res;
		}

		/// <summary>
		/// Определяет дополнительный результат ПСИ-атаки
		/// </summary>
		/// <param name="psi_cast">Результат ПСИ-воздействия</param>
		/// <returns>Дополнительный эффект</returns>
		private string GetPsiEffect(string psi_cast)
		{
			//Результат ПСИ-воздействи:
			//1 - T - Ускорить: юнит получает еще один ход
			//2 - Ps - ПСИ-шит: на юнит кладется отметка щита до конца следующего хода. Для обстрела юнит считается двигавшимтся, юнит не может получать критические паподания.
			//3 - H - Вылечить: восстановить все ОЗ юнита
			//4 - C - Очистить: Снять с юнита одну/все* метку (паника, подавление, отключкаб пси-щит)
			//5 - Sh - Шокировать: следующие 2 хода у юнита доступен только один слот действий (метка подавления)
			//6 - B - Неистовство: юнит тратит все слоты действия на стрельбу по ближайшему юниту (метка паники)
			//7 - R - Бегство: юнит тратитвсе слоты действия на перемещене от противника (метка паники)
			//8 - Rd - Бегство с выбросом оружия (метка паники)
			//9 - S - Ступор: обнулить доступные слоты действия (метка отключки)
			//10- Sd - Ступор с выбросом оружия (метка отключки)
			//11- D - Психосоматический ущерб: нанести противнику 1 дамаг/смертельный дамаг*
			//12- Uc - Контроль разума: следующий ход юнит будет находиться под контролем.
			//*- Критический каст кладет метку до конца боя
			string bac = string.Empty;

			#region Получаем номер
			int effect = 0;
			Random rnd12 = new Random();

			effect = rnd12.Next(0, 12);
			#endregion Получаем номер

			switch (psi_cast)
			{
				case "Себе*":
					bac = string.Format(" - {0:s} ({1:s})\n", Psi_effects[effect].Name, Psi_effects[effect].Critdescripter);
					break;
				case "Себе":
					bac = string.Format(" - {0:s} ({1:s})\n", Psi_effects[effect].Name, Psi_effects[effect].Descripter);
					break;
				case "Промах":
					bac = string.Format(" - {0:s} ({1:s})\n", Psi_effects[effect].Name, Psi_effects[effect].Descripter);
					break;
				case "Получилось":
					bac = string.Format(" - {0:s} ({1:s})\n", Psi_effects[effect].Name, Psi_effects[effect].Descripter);
					break;
				case "Получилось*":
					bac = string.Format(" - {0:s} ({1:s})\n", Psi_effects[effect].Name, Psi_effects[effect].Critdescripter);
					break;
				default:
					bac = string.Empty;
					break;
			}

			return bac;
		}
		#endregion Методы
	}

	/// <summary>
	/// Элемент результата броска Д12
	/// </summary>
	class D12Result
	{

		/// <summary>
		/// Хороший вариант
		/// Чем меньше ResWorth, тем лучше результат
		/// </summary>
		public D12Simpl Good { get; set; }

		/// <summary>
		/// Плохой вариант
		/// Чем больше ResWorth, тем хуже результат
		/// </summary>
		public D12Simpl Bad { get; set; }

		public D12Result()
		{
			Good = new D12Simpl();
			Bad = new D12Simpl();
		}

		/// <summary>
		/// Инициализируем
		/// </summary>
		/// <param name="gt">Название хорошего результата</param>
		/// <param name="gw">Хорошесть хорошего результата</param>
		/// <param name="bt">Название хреновго результата</param>
		/// <param name="bw">Хреновость плохого результата</param>
		public D12Result(string gt, int gw, string bt, int bw)
		{
			Good = new D12Simpl(gt, gw);
			Bad = new D12Simpl(bt, bw);
		}
	}

	/// <summary>
	/// Результат, половинка результата
	/// </summary>
	class D12Simpl
	{
		/// <summary>
		/// Название:
		/// Г - Колова
		/// К - Корпус
		/// Р - Рука
		/// Н - Нога
		/// П - Пусто
		/// </summary>
		public string ResType { get; set; }

		/// <summary>
		/// Степень худшести
		/// чем больше, тем хуже. 
		/// Например, "Пусто" - наихудший из всех возможных,
		/// а "Голова" - самый нормальный
		/// 0 - Г
		/// 1 - К
		/// 2 - Н
		/// 3 - Р
		/// 4 - П
		/// </summary>
		public int ResWorth { get; set; }

		/// <summary>
		/// Constructer
		/// </summary>
		public D12Simpl()
		{
			ResType = string.Empty;
			ResWorth = 0;
		}

		/// <summary>
		/// Констурктор с инициализацией
		/// </summary>
		/// <param name="_t">Название типа</param>
		/// <param name="_w">Худшесть</param>
		public D12Simpl(string _t, int _w)
		{
			ResType = _t;
			ResWorth = _w;
		}

		/// <summary>
		/// Клонировать текущий семпл
		/// </summary>
		/// <returns>Клон текущего экземпляра</returns>
		public D12Simpl Clone()
		{
			return new D12Simpl(this.ResType, this.ResWorth);
		}
	}

	/// <summary>
	/// ПСИ-эффект
	/// </summary>
	class D12PsiEffect
	{
		#region Поля
		/// <summary>
		/// Название эффекта
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Короткое обозначение эффекта
		/// </summary>
		public string Shotrname { get; set; }

		/// <summary>
		/// Расшифровка эффекта
		/// </summary>
		public string Descripter { get; set; }

		/// <summary>
		/// Тяжесть/Полезность
		/// Отрицательные значения для плохих эффектов
		/// Положительные для хороших
		/// </summary>
		public int Hardness { get; set; }

		/// <summary>
		/// Описание ПСИ-эффекта при критическом результате
		/// </summary>
		public string Critdescripter { get; set; }
		#endregion Поля

		#region Методы
		public D12PsiEffect()
		{
			Name = string.Empty;
			Shotrname = string.Empty;
			Descripter = string.Empty;
			Critdescripter = string.Empty;
			Hardness = 0;
		}

		/// <summary>
		/// Инициализация
		/// </summary>
		/// <param name="n_">Название эффекта</param>
		/// <param name="shn_">Сокращенное название</param>
		/// <param name="d_">Расшифровка</param>
		/// <param name="cd_">Расшифровка при критическом значении</param>
		/// <param name="h_">Тяжесть</param>
		public D12PsiEffect(string n_, string shn_, string d_, string сd_, int h_)
		{
			Name = n_;
			Shotrname = shn_;
			Descripter = d_;
			Critdescripter = сd_;
			Hardness = h_;
		}
		#endregion Методы
	}
}
