using System;
using System.Collections.Generic;

namespace D12
{
    static class VehicleShooting
    {
        public static readonly List<D12Result> Results = new List<D12Result>()
        {
            new D12Result("Мимо", 3, "Мимо", 3),
            new D12Result("Скользящее", 2, "Мимо", 3),
            new D12Result("Обычное", 1, "Мимо", 3),
            new D12Result("Обычное", 1, "Скользящее", 2),
            new D12Result("Критическое", 0, "Скользящее", 2),
            new D12Result("Критическое", 0, "Обычное", 1),
            new D12Result("Критическое", 0, "Критическое", 0),
            new D12Result("Критическое", 0, "Мимо", 3),
            new D12Result("Обычное", 1, "Скользящее", 2),
            new D12Result("Обычное", 1, "Мимо", 3),
            new D12Result("Скользящее", 2, "Мимо", 3),
            new D12Result("Мимо", 3, "Мимо", 3)
        };

    }
}