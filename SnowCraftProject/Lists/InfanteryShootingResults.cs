using System;
using System.Collections.Generic;

namespace D12
{
    static class InfanteryShooting
    {
        public static readonly List<D12Result> Results = new List<D12Result>()
        {
                    new D12Result("Пусто", 4, "Пусто", 4),
                    new D12Result("Голова", 0, "Пусто", 4),
                    new D12Result("Голова", 0, "Голова", 0),
                    new D12Result("Голова", 0, "Корпус", 1),
                    new D12Result("Рука", 3, "Рука", 3),
                    new D12Result("Рука", 3, "Пусто", 4),
                    new D12Result("Корпус", 1, "Рука", 3),
                    new D12Result("Корпус", 1, "Корпус", 1),
                    new D12Result("Корпус", 1, "Пусто", 4),
                    new D12Result("Корпус", 1, "Нога", 2),
                    new D12Result("Нога", 2, "Нога", 2),
                    new D12Result("Нога", 2, "Пусто", 4)
        };
    }
}